﻿using System.Data.Entity.Migrations;
using System.Linq;
using BM.Data.Context;
using BM.Data.Entities;

namespace BM.Data.Repositories
{
    public class PhotoRepository : IPhotoRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public PhotoRepository()
        {
            _dbContext = new ApplicationDbContext();
        }

        public IQueryable<Photo> Get()
        {
            return _dbContext.Photos.AsQueryable();
        }

        public Photo Get(int id)
        {
            return Get().FirstOrDefault(p => p.Id == id);
        }

        public Photo Create(Photo photo)
        {
            _dbContext.Photos.Add(photo);
            _dbContext.SaveChanges();
            return photo;
        }

        public Photo Update(Photo photo)
        {
            _dbContext.Photos.AddOrUpdate(photo);
            _dbContext.SaveChanges();
            return photo;
        }

        public void Delete(Photo photo)
        {
            _dbContext.Photos.Remove(photo);
            _dbContext.SaveChanges();
        }
    }
}
