﻿using System.Linq;
using BM.Data.Entities;

namespace BM.Data.Repositories
{
    public interface IPhotoRepository
    {
        IQueryable<Photo> Get();
        Photo Get(int id);
        Photo Create(Photo photo);
        Photo Update(Photo photo);
        void Delete(Photo photo);
    }
}
