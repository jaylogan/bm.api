﻿using System.Linq;
using BM.Data.Entities;

namespace BM.Data.Repositories
{
    public interface IUserRepository
    {
        IQueryable<User> Get();
        User Get(int id);
        User Create(User user);
        User Update(User user);
        void Delete(User user);
    }
}
