﻿using System.Data.Entity.Migrations;
using System.Linq;
using BM.Data.Context;
using BM.Data.Entities;

namespace BM.Data.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public UserRepository()
        {
            _dbContext = new ApplicationDbContext();
        }

        public IQueryable<User> Get()
        {
            return _dbContext.Users.AsQueryable();
        }

        public User Get(int id)
        {
            return Get().FirstOrDefault(u => u.Id == id);
        }

        public User Create(User user)
        {
            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();
            return user;
        }

        public User Update(User user)
        {
            _dbContext.Users.AddOrUpdate(user);
            _dbContext.SaveChanges();
            return user;
        }

        public void Delete(User user)
        {
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }
    }
}
