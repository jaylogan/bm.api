﻿using System.Collections.Generic;

namespace BM.Data.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string EmailAdress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ZipCode { get; set; }

        public ICollection<Photo> Photos { get; set; }
    }
}
