﻿using System;

namespace BM.Data.Entities
{
    public class Photo
    {
        public int Id { get; set; }
        public DateTime Birthday { get; set; }
        public string ChildGender { get; set; }
        public string ChildFirstName { get; set; }
        public string ChildLastName { get; set; }
        public string ChildTeam { get; set; }
        public string ChildPhoto { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
    }
}
