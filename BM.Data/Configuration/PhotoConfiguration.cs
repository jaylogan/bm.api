﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using BM.Data.Entities;

namespace BM.Data.Configuration
{
    public class PhotoConfiguration : EntityTypeConfiguration<Photo>
    {
        public PhotoConfiguration()
        {
            ToTable("Photos");
            HasKey(p => p.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.Birthday).IsRequired().HasColumnName("Birthday");
            Property(p => p.ChildGender).IsRequired().HasColumnName("ChildGender").HasColumnType("nvarchar").HasMaxLength(7);
            Property(p => p.ChildFirstName).IsRequired().HasColumnName("ChildFirstName").HasColumnType("nvarchar").HasMaxLength(255);
            Property(p => p.ChildLastName).IsRequired().HasColumnName("ChildLastName").HasColumnType("nvarchar").HasMaxLength(255);
            Property(p => p.ChildTeam).IsRequired().HasColumnName("ChildTeam").HasColumnType("nvarchar").HasMaxLength(255);
            Property(p => p.ChildPhoto).IsRequired().HasColumnName("ChildPhoto").HasColumnType("nvarchar").HasMaxLength(255);

            HasRequired(p => p.User).WithMany(u => u.Photos).HasForeignKey(p => p.UserId).WillCascadeOnDelete(true);
        }
    }
}
