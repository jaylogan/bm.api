﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using BM.Data.Entities;

namespace BM.Data.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("Users");
            HasKey(p => p.Id);
            Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(p => p.EmailAdress).IsRequired().HasColumnName("EmailAdress").HasColumnType("nvarchar").HasMaxLength(255);
            Property(p => p.FirstName).IsRequired().HasColumnName("FirstName").HasColumnType("nvarchar").HasMaxLength(255);
            Property(p => p.LastName).IsRequired().HasColumnName("LastName").HasColumnType("nvarchar").HasMaxLength(255);
            Property(p => p.ZipCode).IsRequired().HasColumnName("ZipCode");
        }
    }
}
