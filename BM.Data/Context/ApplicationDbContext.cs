﻿using System.Data.Entity;
using BM.Data.Configuration;
using BM.Data.Entities;

namespace BM.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("name=ApplicationContext")
        {
            
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Photo> Photos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new PhotoConfiguration());
            base.OnModelCreating(modelBuilder);
        }
    }
}
