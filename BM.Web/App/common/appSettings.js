﻿(function (module) {
    'use strict';

    module.constant('appSettings', {
        serverUrl: "http://localhost:64201/api"
    });
}(angular.module('photoManager')));