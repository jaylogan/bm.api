﻿(function (module) {
    'use strict';
    module.controller('lookupController', ['photoResource', function (photoResource) {
        var vm = this;

        vm.submit = function() {
            photoResource.query({email: vm.email}, function(data) {
                console.log(data);
            });
        }
    }]);
}(angular.module('photoManager')));