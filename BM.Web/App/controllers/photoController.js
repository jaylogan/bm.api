﻿(function (module) {
    'use strict';
    module.controller('photoController', ['photoResource', function (photoResource) {
        var vm = this;

        vm.newPhoto = new photoResource();
        
        vm.submit = function () {
            vm.newPhoto.childphoto = vm.childphoto.name;
            vm.newPhoto.$save();
        }

    }]);
}(angular.module('photoManager')));