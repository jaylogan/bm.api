﻿(function (module) {
    'use strict';

    module.factory('photoResource', ['$resource', 'appSettings', photoResource]);

    function photoResource($resource, appSettings) {
        return $resource(appSettings.serverUrl + "/upload_photo", null, {
            'update': { method: 'PUT' }
        });
    }
}(angular.module('photoManager')));