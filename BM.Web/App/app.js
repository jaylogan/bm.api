﻿(function () {
    var app = angular.module("photoManager", ["ngRoute", "ngResource", "ui.bootstrap"]);

    app.config(function($routeProvider) {
        $routeProvider.when("/home", {
            controller: "homeController",
            controllerAs: "vm",
            templateUrl: "app/templates/home.html"
        });

        $routeProvider.when("/photo", {
            controller: "photoController",
            controllerAs: "vm",
            templateUrl: "app/templates/photo.html"
        });

        $routeProvider.when("/lookup", {
            controller: "lookupController",
            controllerAs: "vm",
            templateUrl: "app/templates/lookup.html"
        });

        $routeProvider.otherwise({ redirectTo: '/home' });

    });
}());