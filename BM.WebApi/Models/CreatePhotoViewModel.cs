﻿using System;

namespace BM.WebApi.Models
{
    public class CreatePhotoViewModel
    {
        
        public string emailadress { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int zipcode { get; set; }

        public DateTime birthday { get; set; }
        public string childgender { get; set; }
        public string childfirstname { get; set; }
        public string childlastname { get; set; }
        public string childteam { get; set; }
        public string childphoto { get; set; }
    }
}