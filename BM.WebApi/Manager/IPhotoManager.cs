﻿using System.Net.Http;

namespace BM.WebApi.Manager
{
    public interface IPhotoManager
    {
        Data.Entities.Photo Add(HttpRequestMessage request);
    }
}
