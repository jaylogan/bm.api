﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using AutoMapper;
using BM.Data.Entities;
using BM.Data.Repositories;
using BM.WebApi.Models;

namespace BM.WebApi.Controllers
{
    public class Upload_PhotoController : ApiController
    {

        private readonly IPhotoRepository _photoRepository;
        private readonly IUserRepository _userRepository;

        public Upload_PhotoController()
        {
            _photoRepository = new PhotoRepository();
            _userRepository = new UserRepository();
        }

        public IHttpActionResult Get()
        {
            //if (!Request.Headers.Contains("Token") && Request.Headers.GetValues("Token").First() != "bnBsX2NvcGFfdGVjaA==")
            //{
            //    return BadRequest();
            //}

            return Ok(_userRepository.Get().Include(u => u.Photos));
        }

        public IHttpActionResult Get(string email)
        {
            return Ok(_userRepository.Get().Where(u => u.EmailAdress == email).Include(u => u.Photos));
        }

        public IHttpActionResult Post(CreatePhotoViewModel model)
        {
            try
            {
                //if (!Request.Headers.Contains("Token") && Request.Headers.GetValues("Token").First() != "bnBsX2NvcGFfdGVjaA==")
                //{
                //    return BadRequest();
                //}

                Mapper.CreateMap<CreatePhotoViewModel, User>();
                Mapper.CreateMap<CreatePhotoViewModel, Photo>();

                var user = Mapper.Map<User>(model);
                var photo = Mapper.Map<Photo>(model);

                user = _userRepository.Create(user);
                photo.UserId = user.Id;
                _photoRepository.Create(photo);

                return Ok(model);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        public IHttpActionResult Delete(int id)
        {
            return Ok();
        }

    }
}
